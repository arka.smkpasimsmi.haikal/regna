<?php 
    if($this->session->userdata('role') !== 'admin')  {
    Redirect('Page/pageNotFound');
    } 
?>
<tbody>
  <?php $i = 1;
  $h = 1;
   foreach ($pesan as $d): 
    if ($d->tipe_pesan=="pengirim" && $d->sampah =='false'): ?>
    <?php $isi = explode(' ', $d->deskripsi); ?>
    <tr>
      <td>
      </td>
      <?php if ($d->favorit=="true"): ?>
        <td class="mailbox-star"><a href="<?= base_url('Feedback/pesanFavorit/'.$d->id.'/kirim/unfavorit') ?>"><i class="fas fa-star text-warning"></i></a></td>
        <?php elseif($d->favorit=="false"): ?>
         <td class="mailbox-star"><a href="<?= base_url('Feedback/pesanFavorit/'.$d->id.'/kirim/favorit') ?>"><i class="fas fa-star text-secondary"></i></a></td> 
      <?php endif ?>
      <td class="mailbox-name"><a href="<?= base_url('Feedback/bacaPesan/'.$d->id) ?>" class="text-dark">Untuk :<span class="text-primary"><?= $d->email ?></span></a></td>
      <td class="mailbox-subject"><b><?= $d->subject ?></b></td>
      <td><?= date('d-m-Y, H:i', strtotime($d->created_dt))  ?></td>
      <td>
        <div class="list-actions">
            <a href="<?= base_url('Feedback/buangPesan/'.$d->id.'/kirim') ?>"><i class="ik ik-trash-2"></i></a>
        </div>
      </td>
  </tr>
  <?php
  endif;
   endforeach; ?>
</tbody>