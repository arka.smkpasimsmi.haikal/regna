<!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details" data-aos="fade-up">
      <div class="row">
      	<?php foreach ($prestasi as $d): ?>
      	<div class="col-12 col-md-6 col-lg-4">
	        <div class="portfolio-details-container card">

	          <div class="owl-carousel portfolio-details-carousel">
	            <img src="<?= base_url('assets/images/prestasi_images/'.$d->foto) ?>" title="" media-simple="true" width="100%" height="300">
	          </div>

	          <div class="portfolio-info">
	            <h3>Tanggal Kejuaraan</h3>
	            <ul>
	              <li><strong>Tanggal</strong>: <?= $d->tanggal_prestasi ?></li>
	            </ul>
	          </div>

	        </div>

	        <div class="portfolio-description" style="margin-top: -30px;">
	          <h2><?= $d->nama_prestasi ?></h2>
	          <p>
	            <?= $d->deskripsi ?>
	          </p>
	        </div>
	    </div>
	<?php endforeach ?>
      </div>
    </section><!-- End Portfolio Details Section
    -->
</div>