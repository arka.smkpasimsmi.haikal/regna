<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class crud extends CI_Model {

	public function get($table) {
		return $this->db->get($table)->result();
	}

	public function insert($data, $table) {
		$this->db->insert($table, $data);
	}

	public function edit($id, $data, $table) {
		$this->db->where($id);
		$this->db->update($table, $data);
	}

	public function delete($id, $table) {
		$this->db->where_in('id', $id);
		$this->db->delete($table);
	}

	public function getById($table,$id)
	{
		return $this->db->get_where($table, $id)->result();
	}

	public function deletePhoto($namafile,$tipe)
	{
		unlink("assets/images/".$tipe.'/'.$namafile);
	}

	public function countAll($table)
	{
		$query = $this->db->query('SELECT * FROM '.$table);
        $total = $query->num_rows();
        return $total;
	}

}    
